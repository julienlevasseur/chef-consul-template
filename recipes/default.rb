#
# Cookbook:: consul-template
# Recipe:: default
#
# Copyright:: 2018, Julien Levasseur, All Rights Reserved.

directory node['consul-template']['install_dir'] do
  recursive true
end

remote_file "#{node['consul-template']['install_dir']}/consul-template.zip" do
  source "https://releases.hashicorp.com/consul-template/#{node['consul-template']['version']}/consul-template_#{node['consul-template']['version']}_linux_#{node['consul-template']['arch']}.zip"
  mode '0755'
  not_if { ::File.exist?("#{node['consul-template']['install_dir']}/consul-template.zip") }
  not_if { ::File.exist?("#{node['consul-template']['install_dir']}/consul-template") }
end

zipfile "#{node['consul-template']['install_dir']}/consul-template.zip" do
  into "#{node['consul-template']['install_dir']}/"
  only_if { ::File.exist?("#{node['consul-template']['install_dir']}/consul-template.zip") }
end

file "#{node['consul-template']['install_dir']}/consul-template.zip" do
  action :delete
end

if Chef::Config[:file_cache_path].include?('kitchen')
  node.save # ~FC075
  file '/tmp/kitchen/nodes/node.json' do
    owner 'root'
    group 'root'
    mode 0755
    content ::File.open("/tmp/kitchen/nodes/#{node.name}.json").read
    action :create
    only_if { ::File.exist?("/tmp/kitchen/nodes/#{node.name}.json") }
  end
end
