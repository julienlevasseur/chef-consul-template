default['consul-template']['version']     = '0.19.5'
default['consul-template']['arch']        = 'amd64'
default['consul-template']['install_dir'] = '/usr/local/bin'
